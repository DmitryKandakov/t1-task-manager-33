package ru.t1.dkandakov.tm.dto.response.user;

import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.dto.response.AbstractUserResponse;
import ru.t1.dkandakov.tm.model.User;

public final class UserRegistryResponse extends AbstractUserResponse {

    public UserRegistryResponse(@Nullable final User user) {
        super(user);
    }

}
