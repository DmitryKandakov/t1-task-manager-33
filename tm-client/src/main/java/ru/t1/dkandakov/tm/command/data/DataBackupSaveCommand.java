package ru.t1.dkandakov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.dto.request.domain.DataBackupSaveRequest;
import ru.t1.dkandakov.tm.enumerated.Role;

public class DataBackupSaveCommand extends AbstractDataCommand {

    public static final String NAME = "backup-save";

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save backup to file";
    }

    @SneakyThrows
    @Override
    public void execute() {
        getDomainEndpoint().saveDataBackup(new DataBackupSaveRequest());
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}