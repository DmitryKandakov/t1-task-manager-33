package ru.t1.dkandakov.tm.command.server;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.command.AbstractCommand;
import ru.t1.dkandakov.tm.enumerated.Role;

import java.net.Socket;

public final class ConnectCommand extends AbstractCommand {

    @Override
    @SneakyThrows
    public void execute() {
        getServiceLocator().getAuthEndpoint().connect();
        final Socket socket = getServiceLocator().getAuthEndpoint().getSocket();
        getServiceLocator().getProjectEndpoint().setSocket(socket);
        getServiceLocator().getTaskEndpoint().setSocket(socket);
        getServiceLocator().getUserEndpoint().setSocket(socket);
        getServiceLocator().getDomainEndpoint().setSocket(socket);
    }

    @Override
    public @Nullable String getName() {
        return "connect";
    }

    @Override
    public @Nullable Role[] getRoles() {
        return null;
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @Nullable String getDescription() {
        return "Connect to Server.";
    }

}