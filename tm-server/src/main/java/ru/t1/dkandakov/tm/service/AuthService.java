package ru.t1.dkandakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.api.service.IAuthService;
import ru.t1.dkandakov.tm.api.service.IPropertyService;
import ru.t1.dkandakov.tm.api.service.IUserService;
import ru.t1.dkandakov.tm.exception.user.AccessDeniedException;
import ru.t1.dkandakov.tm.exception.user.LoginEmptyException;
import ru.t1.dkandakov.tm.exception.user.PasswordEmptyException;
import ru.t1.dkandakov.tm.model.User;
import ru.t1.dkandakov.tm.util.HashUtil;

public final class AuthService extends AbstractService<User, IUserService> implements IAuthService {


    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final IUserService userService;

    public AuthService(
            @NotNull final IPropertyService propertyService,
            @NotNull final IUserService userService
    ) {
        super(userService);
        this.propertyService = propertyService;
        this.userService = userService;
    }


    @NotNull
    @Override
    public User registry(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        return repository.create(login, password, email);
    }

    @Override
    public @NotNull User check(@Nullable String login, @Nullable String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null || user.getLocked()) throw new AccessDeniedException();
        @Nullable String hash = HashUtil.salt(propertyService, password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        return user;
    }

}